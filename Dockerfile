FROM node:16.20.2-buster

WORKDIR /app

COPY . .
ARG dbconnect
ENV CONNECTION_STRING $dbconnect

RUN yarn install

# lancer la commande pour tourner l'env de test cypress
CMD ["yarn","start"]

EXPOSE 3000