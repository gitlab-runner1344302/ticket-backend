module.exports = {
	e2e: {
		baseUrl: 'https://backend--booktrips--9cql68dflxsn.code.run',
		specPattern: 'cypress/integration/*.spec.{js,jsx,ts,tsx}',
		setupNodeEvents(on, config) {
			// implement node event listeners here
		},
	},
}
